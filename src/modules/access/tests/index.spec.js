var assert = require('chai').assert;  // use chai 'assert' assertion library
import access_module from '../index';
let access = access_module;

describe('Access Module tests:', () => {
    describe('Object api/initial values', () => {
        it('module should export an object', () => {
            assert.isObject(access);
        });
        it('should have property "loggedIn" with initial value of false', () => {
            assert.equal(access.loggedIn, false);
        });

        it('should have property "currentUser" with initial value of null', () => {
            console.log('', access.currentUser);
            assert.isNull(access.currentUser);
        });
        it('should have method "login" which returns a default value of false', () => {
            assert.isFunction(access.login);
        });
        it('should have method "logout" with initial value of null', () => {
            console.log('', access.currentUser);
            assert.isNull(access.currentUser);
        });

    });
    describe('Logging in with valid credentials', () => {
        // arrange
        beforeEach(function () {
            // runs before each test in this block
            // console.log('Access Module beforeEach ...');
            if (access) {
                access.loggedIn = false;
                access.currentUser = null;
            }
        });
        const username = "tom";
        const password = "pwdtom";
        // assert
        it('should change loggedIn status to true', () => {
            // act
            const authenticated = access.login(username, password);
            assert.equal(access.loggedIn, true);
        });
        it('should change currentUser to authenticated user', () => {
            const authenticated = access.login(username, password);
            assert.equal(access.currentUser.username, username);
        });
        it('should return true to caller', () => {
            const authenticated = access.login(username, password);
            assert.equal(authenticated, true);
        });
    });
    describe('Logging in with invalid credentials', () => {
        // arrange
        beforeEach(function () {
            // runs before each test in this block
            // console.log('Access Module beforeEach ...');
            if (access) {
                access.loggedIn = false;
                access.currentUser = null;
            }
        });
        const username = "fred";
        const password = "pwdfred";
        it('loginStatus should be false', () => {
            // act
            const authenticated = access.login(username, password);
            // assert
            assert.equal(access.loggedIn, false);
        });
        it('current user should be null', () => {
            // act
            const authenticated = access.login(username, password);
            // assert
            assert.equal(access.currentUser, null);
        });
        it('should return false to caller', () => {
            // act
            const authenticated = access.login(username, password);
            // assert
            assert.equal(authenticated, false);
        });
    });
    describe('Logging out ', () => {
        // arrange

        describe('with user currently logged in', () => {
            beforeEach(function () {
                // set up logged in user
                if (access) {
                    access.loggedIn = true;
                    access.currentUser = {user:"tom",name:"Tom Agnew"};
                }
            });
            it('loginStatus should be false', () => {
                // act
                const authenticated = access.logout();
                // assert
                assert.equal(access.loggedIn, false);
            });
            it('current user should be null', () => {
                // act
                const authenticated = access.logout();
                // assert
                assert.equal(access.currentUser, null);
            });
        });
        describe('with user currently logged out', () => {
                // set up logged out user
                if (access) {
                    access.loggedIn = false;
                    access.currentUser = null;
                }
            it('loginStatus should be false', () => {
                // act
                const authenticated = access.logout();
                // assert
                assert.equal(access.loggedIn, false);
            });
            it('current user should be null', () => {
                // act
                const authenticated = access.logout();
                // assert
                assert.equal(access.currentUser, null);
            });
        });
    });
});