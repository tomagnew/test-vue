//Cliff Hales
//give a login, check against a hash to see if it is valid
//returned logged in or login failed
import $ from 'jquery';
const access = {
    loggedIn: false,
    currentUser: null,
    login(username, password) {
        // verify credentials
        var self = this;
        return $.getJSON(`http://core.easyblend.org/api/login?username=${username}&password=${password}`)
            .done(function (data) {
                console.log(data);
                if (data.user) { // user is found and authenticated
                    self.loggedIn = true;
                    self.currentUser = data.user;
                    console.log("user logged in!", data.user);
                }
                else { // user not authenticated 
                    self.loggedIn = false;
                    self.currentUser = null;
                    console.log("login failed!");
                }
            });
    },
    logout() {
        this.loggedIn = false;
        this.currentUser = null;
        console.log("user logged out!");
    }
}

export default access;
