let assert = require('chai').assert;  // use chai 'assert' assertion library
import $ from 'jquery';
// global.$ = $;
import login_screen_factory from '../login_screen_factory'; // run 'login_screen' module for side effects

let getMockAccess = function () {
    return {
        loginFired: false,
        logoutFired: false,
        login(username, password) {
            if (username && password) {
                this.loginFired = true;
            }
        },
        logout() {
            this.logoutFired = true;
        }
    };
};

let getMockShowMemberArea = function (mockAccess) {
    let mock = function (mockAccess) {
        let self = this;
        this.fired = false;
        this.accessParamPassed = false;
        this.showMemberArea = function (fakeAccess) {
            console.log('showMemberAreaFired(fakeAccess)', fakeAccess);
            if (fakeAccess == mockAccess) {
                self.fired = true;
                self.accessParamPassed = true;
            }
        };
    };
    return new mock(mockAccess);
};

let mockAccess;
let mockShowMemberArea;
let login_screen;

// arrange

describe('Login Screen module unit tests:', () => {

    describe('Button click handlers', () => {
        // arrange
        beforeEach(function () {
            // runs before each test in this block
            mockAccess = getMockAccess();
            mockShowMemberArea = getMockShowMemberArea(mockAccess);
            login_screen = login_screen_factory({ loginHtml: "test html", access: mockAccess, showMemberArea: mockShowMemberArea.showMemberArea });
        });

        // test login button
        it('clicking login button should call access.login method with username & password', () => {
            // arrange
            // act
            login_screen.btnLogin_click("tom", "pwdtom");
            // assert
            assert.equal(mockAccess.loginFired, true);
        });
        it('clicking login button should call showMemberArea method', () => {
            // arrange
            // act
            login_screen.btnLogin_click("tom", "pwdtom");
            // assert
            assert.equal(mockShowMemberArea.fired, true);
        });

        // test logout button
        it('Clicking logout button should call access.logout method', () => {
            // act
            login_screen.btnLogout_click();
            // assert
            assert.equal(mockAccess.logoutFired, true);
        });
        it('Clicking logout button should call showMemberArea method', () => {
            // act
            login_screen.btnLogout_click("tom", "pwdtom");
            // assert
            assert.equal(mockShowMemberArea.fired, true);
        });
    });
});