console.log('login_screen module running...');
// import $ from 'jquery';
// imports login.html form into dom
const loginHtml = require("html-loader!./login.html");

import access from "../access";
import showMemberArea from '../member_area';
import loginMethods from './login_screen_factory';
// console.log(loginHtml);

let login_module = loginMethods({
    loginHtml,
    access,
    showMemberArea
});

export default login_module;