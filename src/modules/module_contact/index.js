import $ from 'jquery';
import contactPageHtml from 'html-loader!./contact.html';

// load member area content to hidden div
$("#contact_page").html(contactPageHtml).hide();

// exports function which shows area if user logged in, hides otherwise
export default null;
