// import dependencies
import $ from 'jquery';
let viewIds = [
    '#home_page',
    '#members_page',
    '#about_page',
    '#contact_page'
];

let menuLinks = [
    'home',
    'members',
    'about',
    'contact'
];

$('#login').hide(); // hide login form

// export module api

export default {
    display(view) {
        this.hideAllViews();
        $(view).fadeIn();
    },
    hideAllViews() {
        viewIds.forEach(v => {
            // console.log(v);
            $(v).hide();
        });
    },
    updateMenuLinks(menuLink) {
        console.log(menuLink);
        menuLinks.forEach(ml => {
            let $menuLink = $('a[' + ml + ']');
            // console.log(ml, menuLink, ml == menuLink);
            // console.log($menuLink);
            if (ml == menuLink) {
                $menuLink.parent().addClass('active');
            }
            else {
                $menuLink.parent().removeClass('active');
            }
        });
    },
    changeView({ menuLink, viewId, setup }) {
        this.updateMenuLinks(menuLink);
        if (setup) {
            setup();
        }
        this.hideAllViews();
        this.display(viewId);
    }
}
