import $ from 'jquery';
import aboutPageHtml from 'html-loader!./about.html';

// load member area content to hidden div
$("#about_page").html(aboutPageHtml).hide();

// exports function which shows member area if user logged in, hides otherwise
export default null;
